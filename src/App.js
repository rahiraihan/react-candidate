import React from 'react';
import logo from './logo.svg';
import './App.css';
import {Redirect} from "react-router";
import {Router, Switch, Route, BrowserRouter} from 'react-router-dom';
import Candidate from "../src/components/candidate/Candidate";
import AddCandidate from "../src/components/candidate/AddCandidate";
// import Accordions from "../src/components/candidate/Accordions";

function App() {
  return (
    // <Router>
    //     <Switch>
    //         <Route path="/candidate" component={Candidate} exact/>
    //     </Switch>
    // </Router>

<BrowserRouter>
{/* <Candidate/> */}
{/* <AddCandidate/> */}
{/* <Demo/> */}
<Route path = "/candidate" component = {Candidate} exact/>
<Route path = "/addCandidate" component = {AddCandidate} exact/>
{/* <Route path = "/accordions" component = {Accordions} exact/> */}
</BrowserRouter>
);
}

export default App;
