import axios from "axios";

const API_SERVER = "https://localhost:44350/api/Candidate"

export default class CandidateService {

    static getCandidateList(payload) {
        return axios({
            url: `${API_SERVER}/GetCandidateList`,
            method: "GET",
            data: payload
        });
    }
}
