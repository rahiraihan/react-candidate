import React, {Component} from 'react';
import DateTimePicker from 'material-ui-datetimepicker';
import DatePickerDialog from 'material-ui/DatePicker/DatePickerDialog'
import TimePickerDialog from 'material-ui/TimePicker/TimePickerDialog';

class Demo extends Component {
  state = {
    dateTime: null
  }

  setDate = (dateTime) => this.setState({ dateTime })

  render() {
    return (
        <DateTimePicker
        floatingLabelFixed	
        floatingLabelText="Get my date"
        floatingLabelFocusStyle={{ marginTop: '10px' }}
        disabled={false}
        errorText='Required'
        id="some-id"
        fullWidth={false}
      />
    );
  }
}