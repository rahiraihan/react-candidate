import React, {Component} from 'react';
import axios from 'axios';
import { Container, Col, Form, Row, FormGroup, Label, Input, Button } from 'reactstrap';
import { makeStyles,withStyles } from '@material-ui/core/styles';



class AddCandidate extends Component {

    constructor(props) {
        super(props);
        this.state = {
            candidate: {
                candidateId: '',
                candidateFullName: '',
                candidateMobile: '',
                candidateEmail: '',
                candidateAge: '',
                candidateBloodGroup:'',
                candidateAddress:''
            },
           CandidateData:[]
        }
       
    }

    addCandidate=()=>{  
        axios.post('https://localhost:44350/api/Candidate/PostCandidateList', 
        {candidateId:this.state.candidateId,
        candidateFullName:this.state.candidateFullName,  
        candidateMobile:this.state.candidateMobile,
        candidateEmail:this.state.candidateEmail,
        candidateAge:this.state.candidateAge,
        candidateBloodGroup:this.state.candidateBloodGroup,
        candidateAddress:this.state.candidateAddress
})
      .then(json => {  
      if(json.data.Status==='Success'){  
        console.log(json.data.Status);  
        alert("Data Save Successfully");  
        // Candidate.getlist()
    //   this.props.history.push('/Studentlist')  
      }  
      else{  
      alert('Data Saved');  
    //   debugger;  
    //   this.props.history.push('/Studentlist')  
      }  
      })  
      }
      handleChange= (e)=> {  
        this.setState({[e.target.name]:e.target.value});  
        }
      
      
      render() {  
        return (  
           <Container className="App">  
            <h4 className="PageHeading">Enter Candidate Informations</h4>  
           <div id="register">
            <Form className="form">  
              <Col>  
                {/* <FormGroup row>  
                  <Label for="name" sm={2}>Candidate Id</Label>  
                  <Col sm={10}>  
                    <Input type="text" name="candidateId" onChange={this.handleChange} value={this.state.candidateId} placeholder="Enter Candidate Id" />  
                  </Col>  
                </FormGroup>   */}
                <FormGroup row>  
                  <Label for="address" sm={2}>Candidate FullName</Label>  
                  <Col sm={10}>  
                    <Input type="text" name="candidateFullName" onChange={this.handleChange} value={this.state.candidateFullName} placeholder="Candidate FullName" />  
                  </Col>  
                </FormGroup>  
                <FormGroup row>  
                  <Label for="Password" sm={2}>candidateMobile</Label>  
                  <Col sm={10}>  
                    <Input type="text" name="candidateMobile" onChange={this.handleChange} value={this.state.candidateMobile} placeholder="Candidate Mobile" />  
                  </Col>  
                </FormGroup>  
                <FormGroup row>  
                  <Label for="Password" sm={2}>candidateEmail</Label>  
                  <Col sm={10}>  
                    <Input type="text" name="candidateEmail" onChange={this.handleChange} value={this.state.candidateEmail} placeholder="Candidate Email" />  
                  </Col>  
                </FormGroup>  
                <FormGroup row>  
                  <Label for="Password" sm={2}>candidateAge</Label>  
                  <Col sm={10}>  
                    <Input type="text" name="candidateAge" onChange={this.handleChange} value={this.state.candidateAge} placeholder="Candidate Age" />  
                  </Col>  
                </FormGroup>  
                <FormGroup row>  
                  <Label for="Password" sm={2}>candidateBloodGroup</Label>  
                  <Col sm={10}>  
                    <Input type="text" name="candidateBloodGroup" onChange={this.handleChange} value={this.state.candidateBloodGroup} placeholder="Candidate BloodGroup" />  
                  </Col>  
                </FormGroup>  
                <FormGroup row>  
                  <Label for="Password" sm={2}>candidateAddress</Label>  
                  <Col sm={10}>  
                    <Input type="text" name="candidateAddress" onChange={this.handleChange} value={this.state.candidateAddress} placeholder="Candidate Address" />  
                  </Col>  
                </FormGroup>  
              </Col>  
              <Col>  
                <FormGroup row>  
                  <Col sm={5}>  
                  </Col>  
                  <Col sm={1}> 
                  <button type="button" onClick={this.addCandidate} className="button">Submit</button>  
                  </Col>
                  <Col sm={5}>  
                  </Col>  
                </FormGroup>  
              </Col>  
            </Form>  
            </div>
          </Container>  
        );  
        }  
        
}





export default AddCandidate;
